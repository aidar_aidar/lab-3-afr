import unittest

class TestApp(unittest.TestCase):

  def test_div(self):
    import app
    self.assertEqual(app.div(6,3),2)
  def test_prod(self):
    import app
    self.assertEqual(app.prod(2,3),6)
    
if __name__ == "__main__":
  unittest.main()
